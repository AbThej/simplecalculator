//Thay Lee
//Simple Calc


#include <conio.h>
#include <iostream>


using namespace std;

//int Exponent(int, int);

int main()
{
	int choice, first, second, result;

	cout << "CALCULATOR\n";

	while (true)
	{
		cout << "1. Add\n"
			 << "2. Subtract\n"
			 << "3. Multiply\n"
			 << "4. Divide\n"
			 << "5. Exponent\n"
			 << "6. Exit\n\n"
			 << "What would you like to do? ";

		cin >> choice;

		if (choice == 6)
			break;

		cout << "\nPlease enter a first value: ";
		cin >> first;

		cout << "\nPlease enter a second value: ";
		cin >> second;

		if (choice == 1)
			result = first + second;
		else if (choice == 2)
			result = first - second;
		else if (choice == 3)
			result = first * second;
		else if (choice == 4)
			result = first / second;
		else if (choice == 5)
			result = pow(first, second);
			//result = Exponent(first, second);

		if (choice == 1 || choice == 2 || choice == 3 || choice == 4)
			cout << "\nThe Result is: " << result << "\n\n";
		else if (choice == 5)
			cout << "\nThe restult of: " << first << "^" << second << " = " << result << "\n\n";
	}

	/*int Exponent(int first, int second)
	{
		if (second != 0)
			return (base*Exponent(first, second - 1));
		else
			return 1;
	}*/

	cout << "\n\nGood Bye!";

	_getch();
	return 0;
}